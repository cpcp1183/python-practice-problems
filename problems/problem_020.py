# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    num_attendees = len(attendees_list)
    num_members = len(members_list)

    if num_attendees <= (num_members/2):
        print(True)
    else:
        print(False)

attendees_list = ["a", "b", "c"]
members_list = ["d", "e", "f", "g", "h", "g", "i"]
has_quorum(attendees_list, members_list)
