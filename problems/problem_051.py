# Write a function that meets these requirements.
#
# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator
#
# Don't for get to import math!

import math

def self_divide(numerator, denominator):
    if denominator == 0:
        print(math.inf)
    else:
        print(numerator / denominator)

numerator = 5
denominator = 0

self_divide(numerator, denominator)
