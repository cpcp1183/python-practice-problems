# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
# Behavior:
#    * taste(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list
#
# Example:
#    person = Person("Malik",
#                    ["cottage cheese", "sauerkraut"],
#                    ["pizza", "schnitzel"])
#
#    print(person.taste("lasagna"))     # Prints None, not in either list
#    print(person.taste("sauerkraut"))  # Prints False, in the hated list
#    print(person.taste("pizza"))       # Prints True, in the loved list
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

class Person:

    def __init__ (self, name, hated_food, loved_food):
        self.name = name
        self.hated_food = hated_food
        self.loved_food = loved_food

    def taste(self, food):
        if food in hated_food:
            return False
        elif food in loved_food:
            return True
        else:
            return None

person = "Malik"
hated_food = ["cottage cheese", "sauerkraut"]
loved_food = ["pizza", "schnitzel"]
print(tast)

# print(person("lasagna"))     # Prints None, not in either list
# print(person("sauerkraut"))  # Prints False, in the hated list
# print(person("pizza"))       # Prints True, in the loved list
